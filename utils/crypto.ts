// https://github.com/pubkey/eth-crypto/blob/master/tutorials/encrypted-message.md

import EthCrypto from 'eth-crypto';

interface Credencial {
    credential: Record<string, any>;
    issuer: string;
}

interface Apresentacao {
    presentation: Record<string, any>;
    user: string;
}

interface Aberta {
    data: Record<string, any>;
}

function faz_hash(issuer:string, aberta:Aberta ): Credencial {

    return 
}

function encripta(user:string, aberta:Aberta, service:string ): Apresentacao {
    
    return 
}

function decripta(service:string, apresentacao:Apresentacao ): Aberta {
    
    return 
}

function verifica(aberta:Aberta, credencial: Credencial ): boolean {
    
    return 
}


//
// NTT Credential => Credencial
// NTT Presentation => Apresentacao

// NTT Credential - resolvido

// qual informação precisa?
// address to service => _to
// contrato solidity mint (_to , {msg.user}) => tokenid uint
// Apresentação com a informação requisitada
// javascript post https (tokeinid, Apresentacao)
// contrato solidity tokenURI (tokenid) => Apresentacao

